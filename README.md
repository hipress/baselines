In this document, we will briefly introduce how to reproduce the baseline results of `HiPress`, the baseline includes `Ring` (Horovod), `BytePS`, and `Ring(OSS-DGC)`.

### Installing Basic Softwares
MXNet 1.5.1
gluoncv 0.10.1
PyTorch 1.5.0
TensorFlow 1.15.5

Horovod 0.19.2
BytePS 0.2.5

For `Ring(OSS-DGC)`, install Horovod from a pull request [Horoovd-DGC](https://github.com/horovod/horovod/pull/453)

### Start Training
Here, we begin to use the following steps and commands to show how to start training different DNN models atop different frameworks. We will take two AWS EC2 P3dn.24xlarge instances as example.

#### Baseline `Ring` 
Here, we introduce the commands to reproduce the result of `Ring`.
##### MXNet Bert-large
```sh
# Install the gluonnlp first
cd mxnet/bert-large/gluon-nlp
python setup.py install

# Train Bert-large
FI_PROVIDER="efa" RDMAV_FORK_SAFE=1 NUM_GPU=16 BATCH_SIZE_PER_GPU=32 DISTRIBUTED_FRAMEWORK=horovod MODEL=bert_24_1024_16 NCCL_DEBUG=INFO NCCL_TREE_THRESHOLD=0 NCCL_SOCKET_IFNAME=ens5 horovodrun -np 16 -H node1:8,node2:8 --start-timeout 60 --network-interface ens5 bash mxnet/bert-large/baseline-run.sh

```

##### MXNet VGG19
```sh
# Train VGG19
FI_PROVIDER="efa" RDMAV_FORK_SAFE=1 DISTRIBUTED_FRAMEWORK=horovod NCCL_DEBUG=INFO NCCL_TREE_THRESHOLD=0 NCCL_SOCKET_IFNAME=ens5 horovodrun -np 16 -H node1:8,node2:8 --start-timeout 60 --network-interface ens5 python mxnet/vgg-19/train_gluon_imagenet.py --model vgg19 --batch-size 32
```

##### PyTorch LSTM
```sh
FI_PROVIDER="efa" RDMAV_FORK_SAFE=1 DISTRIBUTED_FRAMEWORK=horovod NCCL_DEBUG=INFO NCCL_TREE_THRESHOLD=0 NCCL_SOCKET_IFNAME=ens5 horovodrun -np 16 -H node1:8,node2:8 --start-timeout 60 --network-interface ens5 python pytorch/lstm/main.py --batch_size 80 --epochs 5
```

##### PyTorch UGATIT
```sh
# May download the selfie2anime dataset first
FI_PROVIDER="efa" RDMAV_FORK_SAFE=1 DISTRIBUTED_FRAMEWORK=horovod NCCL_DEBUG=INFO NCCL_TREE_THRESHOLD=0 NCCL_SOCKET_IFNAME=ens5 horovodrun -np 16 -H node1:8,node2:8 --start-timeout 60 --network-interface ens5 python pytorch/ugatit/ugatit/main.py --dataset selfie2anime --dataset_dir trainData_dir/ --batch_size 2
```

##### TensorFlow ResNet50
```sh
FI_PROVIDER="efa" RDMAV_FORK_SAFE=1 DISTRIBUTED_FRAMEWORK=horovod NCCL_DEBUG=INFO NCCL_TREE_THRESHOLD=0 NCCL_SOCKET_IFNAME=ens5 horovodrun -np 16 -H node1:8,node2:8 --start-timeout 60 --network-interface ens5 python tensorflow/resnet-50/benchmark.py
```

##### TensorFlow Transformer
```sh
# Set the python environment and download dataset first as shown in transformer's readme
cd tensorflow/transformer
FI_PROVIDER="efa" RDMAV_FORK_SAFE=1 PYTHONPATH=$PYTHONPATH:baselines/tensorflow/transformer DISTRIBUTED_FRAMEWORK=horovod NCCL_DEBUG=INFO NCCL_TREE_THRESHOLD=0 NCCL_SOCKET_IFNAME=ens5 horovodrun -np 16 -H node1:8,node2:8 --start-timeout 60 --network-interface ens5 python official/r1/transformer/transformer_main.py --ds=horovod --data_dir=trainData_dir/trans_data/ --vocab_file=trainData_dir/trans_data/vocab.ende.32768 --param_set=base --bleu_source=trainData_dir/trans_data/newstest2014.en --bleu_ref=trainData_dir/trans_data/newstest2014.de
```

#### Baseline `BytePS`
Here, we introduce the commands to reproduce the result of `BytePS`.

##### MXNet Bert-large

###### Step1: Set the basic environments for `BytePS`

```sh
# On the Scheduler
export DMLC_NUM_WORKER=2
export DMLC_ROLE=scheduler
export DMLC_NUM_SERVER=2
export DMLC_PS_ROOT_URI=IP_ADDR_OF_NODE1
export DMLC_PS_ROOT_PORT=1234
export DMLC_INTERFACE=ens5
bpslaunch

# On each Servers
export DMLC_NUM_WORKER=2
export DMLC_ROLE=server
export DMLC_NUM_SERVER=2
export DMLC_PS_ROOT_URI=IP_ADDR_OF_NODE1
export DMLC_PS_ROOT_PORT=1234
export DMLC_INTERFACE=ens5
bpslaunch

# On each Workers
export DMLC_NUM_WORKER=2
export DMLC_ROLE=worker
export DMLC_NUM_SERVER=2
export DMLC_PS_ROOT_URI=IP_ADDR_OF_NODE1
export DMLC_PS_ROOT_PORT=1234
export DMLC_INTERFACE=ens5
export NVIDIA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
export DISTRIBUTED_FRAMEWORK=byteps

# Set workerid for every worker one by one
export DMLC_WORKER_ID=0 # on node1
export DMLC_WORKER_ID=1 # on node2
```

###### Step2: Launch the training commands
```sh
bpslaunch bash mxnet/bert-large/baseline-run.sh 
```

##### MXNet VGG19
###### Step1: Set basic environments as set for `MXNet Bert-large`
###### Step2: Launch the training commands
```sh
bpslaunch python mxnet/vgg-19/train_gluon_imagenet.py --model vgg19 --batch-size 32
```
##### PyTorch LSTM
###### Step1: Set basic environments as set for `MXNet Bert-large`
###### Step2: Launch the training commands
```sh
bpslaunch python pytorch/lstm/main.py --batch_size 80 --epochs 5
```
##### PyTorch UGATIT
###### Step1: Set basic environments as set for `MXNet Bert-large`
###### Step2: Launch the training commands
```sh
bpslaunch python pytorch/ugatit/ugatit/main.py --dataset selfie2anime --dataset_dir trainData_dir/ --batch_size 2
```
##### TensorFlow ResNet50
###### Step1: Set basic environments as set for `MXNet Bert-large`
###### Step2: Launch the training commands
```sh
bpslaunch python tensorflow/resnet-50/benchmark.py
```
##### TensorFlow Transformer
###### Step1: Set basic environments as set for `MXNet Bert-large`
###### Step2: Launch the training commands
```sh
export PYTHONPATH=$PYTHONPATH:baselines/tensorflow/transformer
cd tensorflow/transformer
bpslaunch python official/r1/transformer/transformer_main.py --ds=horovod --data_dir=trainData_dir/trans_data/ --vocab_file=trainData_dir/trans_data/vocab.ende.32768 --param_set=base --bleu_source=trainData_dir/trans_data/newstest2014.en --bleu_ref=trainData_dir/trans_data/newstest2014.de
```

#### Baseline `Ring(OSS-DGC)`
Install the related Horovod, then launch the relevant commands like showing above.